﻿using PowerPlant.Interfaces;
using PowerPlant.JsonFile;
using PowerPlantCLI.IoHelpers;

namespace PowerPlantCLI.JsonFile
{
   public class AdminDataSettings : IAdminDataSettings
   {
       private IImportExportJsonFile _importExportJsonFile;
       public AdminDataSettings(IImportExportJsonFile importExportJsonFile)
       {
           _importExportJsonFile = importExportJsonFile;
       }
        public void GetNewDataSettings()
        {
            var fileData= new SettingsFileData();
            fileData.RefreshingTime = ConsoleReadHelper.GetNumber("New refreshing time: ");
            fileData.AcceptableDeviation = ConsoleReadHelper.GetNumber("Acceptable deviation: ");
            ImportExportJsonFile.SettingsFileData = fileData;
            _importExportJsonFile.ExportDataToFile(fileData);          
        }    
    }
}
