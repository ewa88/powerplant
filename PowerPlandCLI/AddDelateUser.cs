﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using PowerPlant.Dtos;
using PowerPlant.Interfaces;
using PowerPlant.Services;
using PowerPlantCLI.IoHelpers;

namespace PowerPlantCLI
{
    public class AddDelateUser : IAddDelateUser
    {
        private IUserService _userService;
        public AddDelateUser(IUserService userService)
        {
            _userService = userService;

        }
        public void AddIfAdminNotExist()
        {
            int count = 0;
            var userList = _userService.GetAllUsers();
            foreach (var user in userList)
            {
                if (user.IsAdmin)
                {
                    count++;
                }
            }
            if (count == 0)
            {
                var admin = new UserDto
                {
                    Login = "Admin",
                    Password = "admin123",
                    IsAdmin=true
                };
                _userService.AddUser(admin);
            }
        }
        public void AddUser()
        {
            bool loop = true;
            while (loop)
            {
                string exit = "";
                string admin = "";
                var user = new UserDto();
                string text = "User name(if you want to stop enter exit):";
                Console.WriteLine(text);
                exit = ConsoleReadHelper.GetString(text);
                if (exit == "exit")
                {
                    loop = false;
                }
                else
                {
                user.Login=exit;
                string nextText = "Password";
                Console.WriteLine(nextText);
                user.Password = ConsoleReadHelper.GetString(nextText);
                string adminText = "User is admin (yes/no) ?";
                Console.WriteLine(adminText);
                admin = ConsoleReadHelper.GetString(adminText);
                user.IsAdmin = ConsoleReadHelper.GetAdminInfo(admin, user);
                _userService.AddUser(user);
            }
        }
        }

        public void RemoveUser(int adminId)
        {
            int id = 0;
            string adminPassword = "";
            string choice;
            bool succes = true;
            bool mainLoop = true;
            while (mainLoop)
            {
                Console.WriteLine("Avaliable users:");
                var userList = _userService.GetAllUsers();
                foreach (var user in userList)
                {
                    Console.WriteLine(user.Id + ". " + user.Login);
                }
                Console.WriteLine("Choose user Id to delete (if you want to stop enter exit): ");
                choice = Console.ReadLine();
                if (choice == "exit" || choice == "Exit")
                {
                    mainLoop = false;
                }
                else
                {
                    succes = Int32.TryParse(choice, out id);
                    bool removeLoop = true;
                    while (removeLoop)
                    {
                        if (succes && _userService.CheckIfUserExistById(id))
                        {
                            if (adminId == id)
                            {
                                Console.WriteLine("Error, you can't delete your profile");
                                removeLoop = false;
                            }
                            else
                            {
                                bool passwordLopp = true;

                                Console.WriteLine("Password:");
                                adminPassword = Console.ReadLine();
                                while (passwordLopp)
                                {
                                    if (_userService.GetPasswordByUserId(adminId, adminPassword))
                                    {
                                        if (_userService.RemoveUser(id))
                                        {
                                            Console.WriteLine("The user {0} has been deleted", id);
                                            passwordLopp = false;
                                            removeLoop = false;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Password:");
                                        adminPassword = Console.ReadLine();
                                    }
                                }
                            }
                        }
                        else
                            {
                                while (!succes || !_userService.CheckIfUserExistById(id))
                                {
                                    Console.WriteLine("Error, choose right Id: ");
                                    choice = Console.ReadLine();
                                    succes = Int32.TryParse(choice, out id);
                                }
                            
                        }
                    }
                }
            }
        }
    }
}
