﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PowerPlantCLI
{
   public  class GetDateUsingRegex : IGetDateUsingRegex
   {
      
        public DateTime GetDateFromRegex(string message)
        {
            string firstDate = "";
            DateTime fromDate = DateTime.Now;
            Console.WriteLine(message);
            bool loop = true;
            while (loop)
            {
                Regex regex = new Regex(@"^\d{4}\/\d{2}\/\d{2} \d{2}|\:\d{2}");

                firstDate = Console.ReadLine();
                if (!regex.IsMatch(firstDate))
                {
                    Console.WriteLine("Error, pattern: 2017/08/25 12:30 ");

                }
                else
                {
                    loop = false;
                    fromDate = DateTime.Parse(firstDate);
                }
            }
            return fromDate;
        }
    }
}
