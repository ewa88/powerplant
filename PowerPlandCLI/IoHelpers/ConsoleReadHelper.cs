﻿using System;
using System.Data.SqlTypes;
using PowerPlant.Dtos;

namespace PowerPlantCLI.IoHelpers
{
    internal class ConsoleReadHelper
    {
        public static int GetNumber(string message)
        {
            int number;
            Console.WriteLine(message);

            while (!int.TryParse(Console.ReadLine(), out number) || number<0)
            {
                Console.WriteLine("Wrong answer, try again.");
            }
          
            return number;
        }
        public static string GetString(string text)
        {
            string userData = Console.ReadLine();
            bool loop = true;
            while (loop)
            {
                if (!string.IsNullOrEmpty(userData))
                {
                    loop = false;
                }
                else
                {
                    Console.WriteLine(text);
                    userData = Console.ReadLine();
                }
            }
            return userData;
        }
        public static bool GetAdminInfo(string admin, UserDto user)
        {
            bool check = true;
            while (check)
            {
                if (admin == "yes" || admin == "no")
                {
                    if (admin == "yes")
                    {
                        user.IsAdmin = true;
                    }
                    if (admin == "no")
                    {
                        user.IsAdmin = false;
                    }
                    check = false;
                }

                else
                {
                    Console.WriteLine("Wrong answer-write yes or no.");
                    admin = Console.ReadLine();
                    check = true;
                }
            }

            return user.IsAdmin;
        }
    }
}