﻿using PowerPlant.Dtos;

namespace PowerPlantCLI
{
    public interface ILogin
    {
        bool CheckIfUserIsAdmin(int userId);
        UserDto Log();
    }
}