using System;
using PowerPlant.Dtos;

namespace PowerPlantCLI
{
    public interface IGetPowerPlantData
    {
        void LoadData(UserDto userDto);
        void LoadTotalData();
        void PrintAdittionPowerPlantData(Object sender, EventArgs e);
        void CheckIfIsBigger(double currentValue, double minValue, double maxValue, double typicalValue);
    }
}