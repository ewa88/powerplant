﻿using System.Reflection;

namespace PowerPlantCLI
{
    public interface IGeneratePowerPlantCalculation
    {
        void LoadData();

        void CheckIfIsBigger(double currentValue, double minValue, double maxValue, double typicalValue,
            PropertyInfo propertyInfo, string name);
    }
}