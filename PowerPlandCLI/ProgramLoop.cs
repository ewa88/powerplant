﻿using System;
using System.Collections.Generic;
using System.Threading;
using PowerPlant.Interfaces;
using PowerPlant.JsonFile;
using PowerPlant.Services;
using PowerPlantCLI.JsonFile;

namespace PowerPlantCLI
{
  internal class ProgramLoop
  {
      private IImportExportJsonFile _importExportJsonFile;
      private IGeneratePowerPlantCalculation _generatePowerPlantCalculation;
      private IGetAlertsFromDb _getAlertsFromDb;
      private IGetPowerPlantData _getPowerPlantData;
      private IAdminDataSettings _adminDataSettings;
      private ILogin _login;
      private IAddDelateUser _addDelateUser;

        public ProgramLoop(IImportExportJsonFile importExportJsonFile, IGeneratePowerPlantCalculation generatePowerPlantCalculation, IGetAlertsFromDb getAlertsFromDb, 
            IGetPowerPlantData getPowerPlantData, IAdminDataSettings adminDataSettings, ILogin login, IAddDelateUser addDelateUser)
        {
            _importExportJsonFile = importExportJsonFile;
            _generatePowerPlantCalculation = generatePowerPlantCalculation;
            _getAlertsFromDb = getAlertsFromDb;
            _getPowerPlantData = getPowerPlantData;
            _adminDataSettings = adminDataSettings;
            _addDelateUser = addDelateUser;
            _login = login;
        }
        public void Execute()
        {
            _importExportJsonFile.GetDataFromFile();        
            Thread t = new Thread(() => _generatePowerPlantCalculation.LoadData());
            t.Start();           
            var user = _login.Log();
            if (_login.CheckIfUserIsAdmin(user.Id))
            {        
                var exit = false;
                while (!exit)
                {

                    Console.WriteLine("Choose command (AddUser, RemoveUser, ChangeSettings, PrintData, PrintTotalEnergy, ExportAlerts, Exit) ");
                    var choice = Console.ReadLine();
                    switch (choice)
                    {
                        case "AddUser":
                            _addDelateUser.AddUser();
                            break;
                        case "RemoveUser":
                           _addDelateUser.RemoveUser(user.Id);
                            break;
                        case "ChangeSettings":
                            _adminDataSettings.GetNewDataSettings();
                            break;
                        case "PrintData":
                            _getPowerPlantData.LoadData(user);
                            break;
                        case "PrintTotalEnergy":
                            _getPowerPlantData.LoadTotalData();
                            break;
                        case "ExportAlerts":
                           _getAlertsFromDb.GetAllerts();
                            break;
                        case "Exit":
                            exit = true;
                            break;
                        case "exit":
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("Command " + choice + " is not supported");
                            break;
                    }
                }
            }
            if(!_login.CheckIfUserIsAdmin(user.Id))
            {            
                var exit = false;
                while (!exit)
                {
                    Console.WriteLine("Choose command (PrintData, PrintTotalEnergy, ExportAlerts, Exit): ");
                    var choice = Console.ReadLine();
                    switch (choice)
                    {
                        case "PrintData":
                            _getPowerPlantData.LoadData(user);
                            break;
                        case "PrintTotalEnergy":
                            _getPowerPlantData.LoadTotalData();
                            break;
                        case "ExportAlerts":
                            _getAlertsFromDb.GetAllerts();
                            break;
                        case "Exit":
                            exit = true;
                            break;
                        case "exit":
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("Command " + choice + " is not supported");
                            break;
                    }
                }
            }
        }
    }
}
