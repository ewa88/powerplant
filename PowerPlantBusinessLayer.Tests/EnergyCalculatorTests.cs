﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PowerPlant.JsonFile;
using PowerPlant.Services;


namespace PowerPlantBusinessLayer.Tests
{
    [TestClass]
    public class EnergyCalculatorTests
    {
        [TestMethod]
        public void SumTotalEnergy_PowerInput_SumOfEnergyOutput()
        {
           double currentEnergy = 1000;
           int intervalTime = 250;
           double sumOfEnergy= currentEnergy * intervalTime / (1000 * 60 * 60);
           var calculationEnergy=new EnergyCalculator();
           calculationEnergy.CalculateEnergySum(currentEnergy, intervalTime);
           double sum = calculationEnergy.GetSumOfEnergy();

           Assert.AreEqual(sumOfEnergy,sum);
        }
    }
}
