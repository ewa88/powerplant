﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _PowerPlantAlertsAssets
{
  public class ProgramLoop
    {
        public void Execute()
        {
            var import=new ImportDataFromJson();
           var array= import.GetData();
            if (array != null)
            {
                var print = new PrintDataFromArray();
                print.PrintData(array);
            }

        }
    }
}
