﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlantDataLayer.Repositories;

namespace PowerPlantDataLayer.NinjectModule
{
   public class RepositoryModule:Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IAlertRepository>().To<AlertRepository>();
            Bind<IUserRepository>().To<UserRepository>();
        }
    }
}
