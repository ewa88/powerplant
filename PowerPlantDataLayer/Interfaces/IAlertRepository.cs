﻿using System;
using System.Collections.Generic;
using PowerPlantDataLayer.Models;

namespace PowerPlantDataLayer.Repositories
{
    public interface IAlertRepository
    {
        bool AddAlert(AlertOfCrossing alert);
        List<AlertOfCrossing> GetPeriodDataFromDb(DateTime from, DateTime to);
    }
}