using System.Collections.Generic;
using PowerPlantDataLayer.Models;

namespace PowerPlantDataLayer.Repositories
{
    public interface IUserRepository
    {
        bool AddUsers(User user);
        List<User> GetAllUsers();

        bool IsUserInDb(string login)//wyrzuci�am static bo metoda by�a niewidoczna
            ;

        bool CheckIfUserExistById(int id);
        bool RemoveUser(int id);
        bool CheckIfPasswordIsCorrect(int id, string password);
    }
}