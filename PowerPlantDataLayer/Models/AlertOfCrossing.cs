﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerPlantDataLayer.Models
{
   public class AlertOfCrossing
    {
        public int Id { get; set; }
        public string DeviceName { get; set; }
        public string ParameterName { get; set; }
        public DateTime TimeOfOccurrence { get; set; }
        public string LoggedUser { get; set; }
    }
}
