﻿using System.Configuration;
using System.Data.Entity;
using PowerPlantDataLayer.Models;

namespace PowerPlantDataLayer.DbContext
{
    internal class PowerPlantDbContext : System.Data.Entity.DbContext
    {

        public PowerPlantDbContext() : base(GetConnectionString())
        { }

        public DbSet<User> UsersDbSet { get; set; }
        public DbSet<AlertOfCrossing> AlertOfCrossingDbSet{ get; set; }

    private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["PowerPlantDb"].ConnectionString;
        }
    }
}

