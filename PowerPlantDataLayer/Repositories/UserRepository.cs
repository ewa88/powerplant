﻿using System.Collections.Generic;
using System.Linq;
using PowerPlantDataLayer.DbContext;
using PowerPlantDataLayer.Models;

namespace PowerPlantDataLayer.Repositories
{
   public class UserRepository : IUserRepository
   {
        public bool AddUsers(User user)
        {
            var rowsAffected = 0;
            using (var dbContext = new PowerPlantDbContext())
            {
                dbContext.UsersDbSet.Add(user);
                rowsAffected = dbContext.SaveChanges();
            }
            return (rowsAffected == 1);
        }
        public List<User> GetAllUsers()
        {
            List<User> users;
            using (var dbContext = new PowerPlantDbContext())
            {
                users = dbContext.UsersDbSet.ToList();
            }
            return users;
        }

        public bool IsUserInDb(string login)
        {
            using (var dbContext = new PowerPlantDbContext())
            {
                var userDb = dbContext.UsersDbSet.Count(x => x.Login == login);
                if (userDb == 0)
                    return false;
                return true;
            }
        }
        public bool CheckIfUserExistById(int id)
        {
            using (var dbContext = new PowerPlantDbContext())
            {
                var userDb = dbContext.UsersDbSet.Count(x => x.Id == id);
                if (userDb == 0)
                    return false;
                return true;
            }
        }
        public bool RemoveUser(int id)
        {
            var rowsAffected = 0;
            using (var dbContext = new PowerPlantDbContext())
            {
              var user= dbContext.UsersDbSet.SingleOrDefault(u => u.Id==id);
                dbContext.UsersDbSet.Remove(user);
                rowsAffected = dbContext.SaveChanges();
            }
            return rowsAffected != 0;
        }

        public bool CheckIfPasswordIsCorrect(int id, string password)
        {
            using (var dbContext = new PowerPlantDbContext())
            {
                var userDb = dbContext.UsersDbSet.Where(a=>a.Id==id).Select(p=>p.Password==password).SingleOrDefault();
                if (userDb)
                    return true;
                return false;
            }
        }
    }
}
