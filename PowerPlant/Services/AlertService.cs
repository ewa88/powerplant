﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonHelper;
using PowerPlant.Dtos;
using PowerPlant.Interfaces;
using PowerPlant.Mappers;
using PowerPlantDataLayer.Repositories;

namespace PowerPlant.Services
{
   public class AlertService : IAlertService
   {
       private IAlertRepository _alertRepository;
       public AlertService(IAlertRepository alertRepository)
       {
           _alertRepository = alertRepository;
       }
        public bool AddAlert(AlertOfCrossingDto alert)
        {
            return _alertRepository.AddAlert(DtoToEntityMapper.AlertOfCrossingDtoToEntityModel(alert));
        }
        public List<AlertOfCrossingDto> GetPeriodData(DateTime from, DateTime to)
        {
            var list= EntityToDtoMapper.AlertsListEntityModelToDto(_alertRepository.GetPeriodDataFromDb(from, to));
            return list;
        }
    }
}
