﻿using System;
using PowerPlant.Interfaces;
using PowerPlantDataProvider;


namespace PowerPlant.Services
{
    public class PowerPlantService : IPowerPlantService
    {
        public PowerPlantDataProvider.PowerPlant GetPowerPlant()
        {
            var plant = PowerPlantDataProvider.PowerPlant.Instance;
            return plant;
        }      
    }
}
