﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using PowerPlant.Dtos;
using PowerPlant.Interfaces;
using PowerPlant.Mappers;
using PowerPlantDataLayer.Repositories;

namespace PowerPlant.Services
{
  public class UserService : IUserService
  {
      private IUserRepository _userRepository;

      public UserService(IUserRepository userRepository)
      {
          _userRepository = userRepository;
      }
        public List<UserDto> GetAllUsers()
        {
            var allUsers = new List<UserDto>();
            var users = _userRepository.GetAllUsers();
            foreach (var user in users)
            {
                var userDto = EntityToDtoMapper.UserEntityModelToDto(user);
                allUsers.Add(userDto);
            }
            return allUsers;
        }
        public bool AddUser(UserDto user)
        {
            return _userRepository.AddUsers(DtoToEntityMapper.UserDtoToEntityModel(user));
        }
        public bool CheckIfUserExistById(int id)
        {
            return _userRepository.CheckIfUserExistById(id);
        }
        public bool RemoveUser(int id)
        {
            return _userRepository.RemoveUser(id);
        }
        public bool GetPasswordByUserId(int id, string password)
        {
            return _userRepository.CheckIfPasswordIsCorrect(id, password);
        }
    }
}
