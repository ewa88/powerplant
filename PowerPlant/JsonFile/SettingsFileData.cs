﻿namespace PowerPlant.JsonFile
{
    public class SettingsFileData
    {
        public int RefreshingTime;
        public int AcceptableDeviation;
    }
}