﻿

namespace PowerPlant.Dtos
{
   public class UserDto
    {
        public int Id;
        public string Login;
        public string Password;
        public bool IsAdmin;
    }
}
