using PowerPlantDataProvider;

namespace PowerPlant.Interfaces
{
    public interface IEnergyCalculator
    {
        void CalculateEnergySum(double currentEnergy, int intervalTime);
        double GetSumOfEnergy();
        void ClearSumOfEnergy();
    }
}