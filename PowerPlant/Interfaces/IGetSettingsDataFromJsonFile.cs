namespace PowerPlant.Interfaces
{
    public interface IGetSettingsDataFromJsonFile
    {
        int GetRefreshingTime();
        double GetMinValue(double typicalValue);
        double GetMaxValue(double typicalValue);
    }
}