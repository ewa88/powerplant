﻿using System;
using System.Collections.Generic;
using JsonHelper;

namespace PowerPlant.Interfaces
{
    public interface IAlertService
    {
        bool AddAlert(AlertOfCrossingDto alert);
        List<AlertOfCrossingDto> GetPeriodData(DateTime from, DateTime to);
    }
}