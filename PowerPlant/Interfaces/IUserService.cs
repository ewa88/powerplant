﻿using System.Collections.Generic;
using PowerPlant.Dtos;

namespace PowerPlant.Interfaces
{
    public interface IUserService
    {
        List<UserDto> GetAllUsers();
        bool AddUser(UserDto user);
        bool CheckIfUserExistById(int id);
        bool RemoveUser(int id);
        bool GetPasswordByUserId(int id, string password);
    }
}