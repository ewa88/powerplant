﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlant.Interfaces;
using PowerPlant.JsonFile;
using PowerPlant.Services;

namespace PowerPlant.NinjectModule
{
   public class ServiceModule:Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IAlertService>().To<AlertService>();
            Bind<IGetSettingsDataFromJsonFile>().To<GetSettingsDataFromJsonFile>();
            Bind<IImportExportJsonFile>().To<ImportExportJsonFile>();
            Bind<IPowerPlantService>().To<PowerPlantService>();
            Bind<IUserService>().To<UserService>();
            Bind<IEnergyCalculator>().To<EnergyCalculator>();


        }
    }
}
